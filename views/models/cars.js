var mongoose = require("mongoose");
var carSchema = new mongoose.Schema({
    brand: String,
    model: String,
    number: String,
    image: String,
    price: Number,
    type: String,
    city: String,
    bookingdate:Date,
    retrundate:Date
});

module.exports = mongoose.model("car", carSchema);