var mongoose = require("mongoose"),
    passportLocalMongoose=require("passport-local-mongoose");
var userSchema = new mongoose.Schema({
    username: String,
    fname: String,
    lname: String,
    phone: Number,
    state: String,
    city: String,
    pin: Number,
    location: String,
    password: String,
    dl:String,
    verified:Boolean
});
userSchema.plugin(passportLocalMongoose);
module.exports= mongoose.model("user", userSchema);