var express =require("express");
var Router=express.Router(),
    car=require("../views/models/cars"),
    passport = require("passport"),
    user=require("../views/models/user.js");

    function isloggedin(req,res,next){
        if(req.isAuthenticated()){
           console.log('username',req.user.username);
            if(req.user.username=="admin")
            {
                res.redirect("/admin");
            }
            else{return next();}
            
        }
        else{
            res.redirect("/user/login");
        }
    }
//register user form
Router.get("/register", function (req, res) {
    res.render("User/registerUser.ejs");
})

//register user to db
Router.post("/register", function (req, res) {
    req.body.username
    user.register(new user({ username: req.body.username,
                             phone:req.body.user.phone, 
                             fname:req.body.user.fname,
                             lname:req.body.user.lname,
                             state:req.body.user.state,
                             pin:req.body.user.pin,
                             location:req.body.user.location,
                             city:req.body.user.city}), req.body.password, function (err, us) {
        if (err) {
            console.log("==========Err6=========\n" + err);
         }
         else{
             console.log("=======new user success=======",us);
             res.redirect("/")
         }
        })
})

//user dashboard
Router.get("/dashboard",isloggedin,function(req,res){
    res.render("User/dashboard.ejs",{user:req.user.username});
})



//trip search
Router.post("/dashboard/find",function(req,res){
   console.log(req.body.trip.fromcity);
    car.find({"city":req.body.trip.fromcity},function(err,car){
        if(err)
        {
            console.log("=========err7=====");
        }
        else{
            // console.log(car);
            // console.log(car[0].type);
            res.render("User/trip.ejs",{car:car});
        }
    })
    
})
module.exports=Router;


