var express =require("express");
var Router=express.Router(),
    car=require("../views/models/cars"),
    user=require("../views/models/user.js"),
    bodyparser = require("body-parser");
   // app.use(bodyparser.urlencoded({ extended: true }));
//admin dashboard + registerd cars
function isAdmin(req,res,next){
   // console.log("============req==========",req.user);
    if(req.user.username=="admin")
    {
        return next();
    }
    else{
        res.redirect("/user/login");
    }
}
function isloggedin(req,res,next){
    if(req.isAuthenticated()){
      // console.log('username',req.user.username);
        if(req.user.username=="admin")
        {
            res.redirect("/admin");
        }
        else{return next();}
        
    }
    else{
        res.redirect("/user/login");
    }
}

Router.get("/", isAdmin,function (req, res) {
    car.find({}, function (err, cars) {
        if (err) {
            console.log("=========err2===========\n")
        }
        else {
            res.render("Admin/admin.ejs", { cars: cars });
        }
    })
})
//registered cars
Router.get("/registerCar", isAdmin ,function (req, res) {
    res.render("Admin/registerCar.ejs");
})

//register new car to db
Router.post("/registerCar", isAdmin , function (req, res) {
    car.create(req.body.car, function (err, car) {
        if (err) {
            console.log("==============Err1===========\n" + err);
        }
        else {
            console.log("============Success=========\n" + car);
        }
    })
    res.redirect("/admin");
})


//delete car
Router.delete("/:id", isAdmin , function (req, res) {
    car.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            console.log("=========ERR3======\n" + err);
        }
        else {
            res.redirect("/admin");
        }
    })
})


//edit car form
Router.get("/:id/edit", isAdmin , function (req, res) {
    car.findById(req.params.id, function (err, car) {
        if (err) {
            console.log("============ERR4==========\n" + err);
        }
        else {
            res.render("Admin/editCar.ejs", { car: car });
        }
    })

})


//edit  car to db
Router.put("/:id/edit",  isAdmin , function (req, res) {
    car.findByIdAndUpdate(req.params.id, req.body.car, function (err, car) {
        if (err) {
            console.log("==========Err5===========");
        }
        else {
            console.log("===========success Edit car ========");
            res.redirect("/admin");
        }

    })
})

//registered users
Router.get("/registeredUser",isAdmin, function (req, res) {
    user.find({}, function (err, users) {
        if (err) {
            console.log("========err7=========\n" + err)
        }
        else {
               user.distinct("city", function(err,allcity){
                if(err)
                {
                    console.log("========errr======");
                }
                else{
                //  console.log('distinct: ',allcity);
                  res.render("Admin/registeredUser.ejs", { users:users,allcity:allcity })
                }
             })
            }
    })

})

//check box filter
Router.post("/registeredUser/filter",isAdmin,function(req,res){
    user.find({city:req.body.city},function(err,fcity){
           if(err)
           {
               console.log(err);
           }
           else{
            //    console.log('all data:',fcity);
               user.distinct("city", function(err,allcity){
                  if(err)
                  {
                      console.log("========errr======");
                  }
                  else{
                  //  console.log('distinct: ',allcity);
                    res.render("Admin/registeredUser.ejs", { users:fcity,allcity:allcity })
                  }
               })
             
           }
    })
})

//user filter search
Router.post("/registereduser/search",function(req,res){
    //res.send("getting");
    // user.find({ $or:[{"fname": /bl/i},{"fname": /ka/i} ]},function(err,car){
//     console.log(car);
// })
//$regex : new RegExp(thename, "i")
//"fname":{$regex : new RegExp(req.body.name, "i")}
    console.log(req.body.name);
    user.find({$or:[{"fname":{$regex : new RegExp(req.body.name, "i")}},{"lname":{$regex : new RegExp(req.body.name, "i")}}]},function(err,fuser){
        if(err)
        {
            console.log("=====err in user search==========");
        }
        else{
           // console.log(fuser);
            user.distinct("city", function(err,allcity){
                if(err)
                {
                    console.log("========errr======");
                }
                else{
                //  console.log('distinct: ',allcity);
                  res.render("Admin/registeredUser.ejs", { users:fuser,allcity:allcity })
                }
             })

        }

    })
})


//DL view
Router.get("/:id/dl",function(req,res){
    user.findById(req.params.id,function(err,user){
        if(err)
        {
            console.log("======err11=======",err);
        }
        else{
            res.render("Admin/dl.ejs",{dl:user});
        }
    })
})


//dl approval or denial
Router.post("/:id/dl",function(req,res){
    user.findById(req.params.id,function(err,fuser){
        if(err)
        {
            console.log("========err12=========");
        }
        else{
            if(!fuser.verified)
            {
                user.findByIdAndUpdate(req.params.id,{verified:"true"},function(err,usr){
                    console.log("updated",usr);
                    res.redirect("/admin/registereduser");
                })
            }
            else{
                user.findByIdAndUpdate(req.params.id,{verified:"false"},function(err,usr){
                    console.log("updated",usr);
                    res.redirect("/admin/registereduser");})
            }
           
        }
    })
})
module.exports=Router;