

var express = require("express"),
    bodyparser = require("body-parser"),
    mongoose = require("mongoose"),
    methodOverride = require("method-override"),
    passport = require("passport"),
    localStrategy = require("passport-local"),
    passportLocalMongoose = require("passport-local-mongoose"),
    car = require("./views/models/cars"),
    user = require("./views/models/user.js"),
    tripdb = require("./views/models/trip.js"),
    app = express();
var multer = require('multer');
var upload = multer({ dest: 'public/uploads/' });
var adminR = require("./routes/adminRoutes.js");

app.use(require("express-session")({
    secret: "HOLA",
    resave: false,
    saveUninitialized: false
}))
app.use(bodyparser.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());
mongoose.connect("mongodb://halder:halder@ds241578.mlab.com:41578/carlo");
//mongoose.connect("mongodb://localhost/car");
// app.use('/assets', express.static(__dirname + '/public.{js,html,css,svg}'));
app.use(express.static('public'));
app.use(methodOverride("_method"));
passport.serializeUser(user.serializeUser());
passport.deserializeUser(user.deserializeUser());
passport.use(new localStrategy(user.authenticate()));

//middlewares
function isAdmin(req, res, next) {
    if (req.user.username == "admin") {
        return next();
    }
    else {
        res.redirect("/user/login");
    }
}
function isloggedin(req, res, next) {
    if (req.isAuthenticated()) {
        console.log('username', req.user.username);
        if (req.user.username == "admin") {
            res.redirect("/admin");
        }
        else { return next(); }

    }
    else {
        res.redirect("/user/login");
    }
}
app.use(function (req, res, next) {
    res.locals.cuser = req.user;
    next();
})
//===============================
//routes
//===============================


//main page
app.get("/", function (req, res) {

    res.render("User/home.ejs");
});

app.use('/admin', adminR);

//register user form
app.get("/user/register", function (req, res) {
    res.render("User/registerUser.ejs");
})

//register user to db
app.post("/user/register", upload.any(), function (req, res) {
    //console.log(req.files[0].filename);
    req.body.username
    user.register(new user({
        username: req.body.username,
        phone: req.body.user.phone,
        fname: req.body.user.fname,
        lname: req.body.user.lname,
        state: req.body.user.state,
        pin: req.body.user.pin,
        location: req.body.user.location,
        city: req.body.user.city,
        dl: req.files[0].filename,
        verified: "false"
    }), req.body.password, function (err, us) {
        if (err) {
            console.log("==========Err6=========\n" + err);
        }
        else {
            console.log("=======new user success=======", us);
            res.redirect("/")
        }
    })
})


//user filters checkBox
//user dashboard
app.get("/user/dashboard", isloggedin, function (req, res) {
    car.find({}, function (err, cars) {
        if (err) {
            console.log("=======err10===========");
        } else {
            // console.log(cars);
            res.render("User/dashboard.ejs", { user: req.user.username, cars: cars, city: "all cities", trip: "" });
        }
    })
})


//user login
app.get("/user/login", function (req, res) {
    res.render("User/userlogin.ejs");
})

//user login authen
app.post("/user/login", passport.authenticate("local", {
    successRedirect: "/user/dashboard",
    failureRedirect: "/user/login"
}), function (req, res) {
});

//user logout
app.get("/user/logout", function (req, res) {
    req.logout();
    res.redirect("/");
});

//trip search
var trip = {
    fromcity: '',
    tocity: '',
    fromdate: '',
    todate: '',
    type: '',
    offer: ''
};
app.post("/user/dashboard/find", function (req, res) {
    //console.log(req.body.trip.fromcity);
    car.find({ "city": req.body.trip.fromcity, "type": req.body.trip.type }, function (err, cars) {
        if (err) {
            console.log("=========err7=====");
        }
        else {
            trip = req.body.trip;
            console.log(trip);
            res.render("User/dashboard.ejs", { user: req.user.username, cars: cars, city: req.body.trip.fromcity, trip: req.body.trip });
        }
    })

})

//show car details
app.get("/user/dashboard/:id", isloggedin, function (req, res) {
    car.findById(req.params.id, function (err, c) {
        if (err) {
            console.log("====err10=====");
        }
        else {
            console.log(req.body.trip);

            res.render("User/carDetails.ejs", { car: c, trip: trip });
        }
    })
})

app.post("/user/dashboard/:id", function (req, res) {
    trip = req.body.trip;
    console.log(trip);
    res.redirect("/user/dashboard/" + req.params.id + "/payment");
})

//payment
app.get("/user/dashboard/:id/payment", function (req, res) {
    car.findById(req.params.id, function (err, car) {
        if (err) {
            console.log("==========err=========");
        }
        else {
            var da = [];
            da[0] = trip.fromdate;
            da[1] = trip.todate;
            var d1 = da[0].split("-", 3)[2];
            var d2 = da[1].split("-", 3)[2];
            var m1 = da[0].split("-", 3)[1];
            var m2 = da[1].split("-", 3)[1];
            var y1 = da[0].split("-", 3)[0];
            var y2 = da[1].split("-", 3)[0];
            //current date 
            var today = new Date();
            //console.log(parseInt(m1)+" "+today.getMonth());
            if (parseInt(y1) >= parseInt(today.getFullYear())) {
               // console.log("*1");
                if ((parseInt(m1)) >= (parseInt(today.getMonth()))+1) {
                    //console.log("*2");
                    if (parseInt(d1) >= parseInt(today.getDate())) {
                       // console.log("*3");
                        var days = (parseInt(d2) - parseInt(d1)) + ((parseInt(m2) - parseInt(m1)) * 30) + ((parseInt(y2) - parseInt(y1)) * 365);
                        // console.log(days);
                        res.render("User/payment.ejs", { trip: trip, car: car, d: days });
                    } else {
                        res.render("User/dateError.ejs",{msg:"date"});
                    }
                } else {
                    res.render("User/dateError.ejs",{msg:"month"});
                }
            } else {
               // console.log("4");
                res.render("User/dateError.ejs",{msg:"year"});
            }

            //console.log(today.getDate());

        }
    })


})


//register trip
app.post("/user/dashboard/:car/payment/:usr", function (req, res) {
    
    tripdb.create({
        userid: req.params.usr,
        carid: req.params.car,
        fromcity: trip.fromcity,
        fromdate: trip.fromdate,
        todate: trip.todate,
        amount: req.body.price
    }, function (err, trip) {
        if (err) {
            console.log("======err13=========");
        } else {
            console.log("=======success trip saved======",trip);
            car.findById(req.params.car , function(err,fcar){
                if(err){
                    console.log("======err14=======");
                } else{
                    console.log("search::",fcar);
                }
            })
            car.findByIdAndUpdate(req.params.car,{"bookingdate":trip.fromdate,
            "retrundate":trip.todate},function(err,fcar){
                if(err){
                    console.log("======err14=======");
                }
                else{
                   // fcar.save();
                    console.log(fcar)
                }
            })
            res.redirect("/user/booking/"+req.params.usr)
        }

    })
    // console.log(trip);
    // console.log(req.body.user);
})

//show bookings
app.get("/user/booking/:id", isloggedin, function (req, res) {
    tripdb.find({ userid: req.params.id }, function (err, trip) {
        if (err) {
            console.log("==========err15======");
        }
        else {
            var carid = [];
            trip.forEach(function (t) {
                carid.push(t.carid);
            })
            car.find({ "_id": carid }, function (err, cars) {
                if (err) {
                    console.log("======err15.1==");
                } else {
                    res.render("User/booking.ejs", { cars: cars, trip: trip });
                }
            })

        }
    })
})

//========================Admin==============================
// user.register(new user({username:"admin"}),"admin",function(err,data){
//     if(err)
//     {console.log(err)}
//     else{
//         console.log(data)
//     }    
// })

//=====================find similar data from db================== pass="/b/i"
// var x="b";
// var pass='/b/i';
// console.log(pass);
// user.find({ $or:[ {"fname": /b/i},{"fname": /ka/i} ]},function(err,car){
//     console.log(car);
// })
//==============================
//port
//==============================
app.listen(process.env.PORT||3000, function () {
    console.log("listening for car rent")
})